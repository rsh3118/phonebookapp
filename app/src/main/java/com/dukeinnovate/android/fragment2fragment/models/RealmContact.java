package com.dukeinnovate.android.fragment2fragment.models;

import com.dukeinnovate.android.fragment2fragment.Contact;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import io.realm.MutableRealmInteger;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by shreyasingh on 5/31/18.
 */

public class RealmContact extends RealmObject {
    private static AtomicInteger INTEGER_COUNTER = new AtomicInteger(0);


    @Required
    private String name;

    @PrimaryKey
    @Required
    private String id = UUID.randomUUID().toString();

    @Required
    private String phoneNumber, title, email;

    public final MutableRealmInteger integer = MutableRealmInteger.valueOf(0);

    public RealmContact(String name, String phoneNumber, String title, String email, String ID) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.title = title;
        this.email = email;
        this.id = ID;
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public RealmContact(){

    }



}
