package com.dukeinnovate.android.fragment2fragment;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import java.util.jar.Attributes;

import static android.content.ContentValues.TAG;

/**
 * Created by shreyasingh on 5/31/18.
 */

public class JSONParserr {
    public static Contact parse(String jString){

        try {
            JSONObject jsonObject = new JSONObject(jString);
            String name = jsonObject.getString("name");
            String phoneNumber = jsonObject.getString("phoneNumber");
            String title = jsonObject.getString("title");
            String email = jsonObject.getString("email");
            String ID = jsonObject.getString("ID");
            Log.d(TAG, "parse: " + name + phoneNumber + title + email);
            Contact contact = new Contact(name, phoneNumber, title, email, ID);
            Log.d(TAG, "parse: " + contact.getName() + contact.getPhoneNumber() + contact.getTitle() + contact.getEmail());
            return contact;


//            JSONArray contactsFromRealm = jsonObject.getJSONArray("contactsFromRealm");
//            ArrayList<Contact> contactsList = new ArrayList<>();
//
//
//            //for (int i = 0; i < contactsFromRealm.length(); i++){
//                JSONObject c = contactsFromRealm.getJSONObject(0);
//                String name = c.getString("name");
//                Log.d(TAG, "parse: " + name);
//
//                String phoneNumber = c.getString("phoneNumber");
//
//                Log.d(TAG, "parse: " + phoneNumber);
//                String title = c.getString("title");
//
//                Log.d(TAG, "parse: " + title);
//                String email = c.getString("email");
//
//                Log.d(TAG, "parse: " + email);
//
//                Contact contact = new Contact(name, phoneNumber, title, email);
//            Log.d(TAG, "parse: contact " + contact.getName() + contact.getPhoneNumber());
//
//            JSONObject b = contactsFromRealm.getJSONObject(1);
//            String name1 = b.getString("name");
//            Log.d(TAG, "parse: " + name);
//
//            String phoneNumber1 = b.getString("phoneNumber");
//
//            Log.d(TAG, "parse: " + phoneNumber);
//            String title1 = b.getString("title");
//
//            Log.d(TAG, "parse: " + title);
//            String email1 = b.getString("email");
//
//            Log.d(TAG, "parse: " + email);
//
//            Contact contact1 = new Contact(name1, phoneNumber1, title1, email1);
//            Log.d(TAG, "parse contact1: " + contact1.toString());

//            ArrayList<Contact> contactsArrayList = new ArrayList<>();
//            contactsArrayList.add(contact);
//            contactsArrayList.add(contact1);





            //}

        } catch (final JSONException e){
            e.printStackTrace();
        }
        return null;
//        try {
//            JSONObject all = new JSONObject(jString);
//
//            String mName = all.getString("name");
//            String mPhoneNumber = all.getString("phoneNumber");
//            String mTitle = all.getString("title");
//            String mEmail = all.getString("email");
//
//        } catch (JSONException e){
//            e.printStackTrace();
//        }
//        return null;

    }
}
