package com.dukeinnovate.android.fragment2fragment;

import com.dukeinnovate.android.fragment2fragment.models.RealmContact;

import java.util.ArrayList;

import io.realm.RealmList;

/**
 * Created by shreyasingh on 6/11/18.
 */

public interface OnContactSelectedListener {
    public void onContactSelected(Integer position, RealmList<RealmContact> contactsArray);
}
