package com.dukeinnovate.android.fragment2fragment;

/**
 * Created by shreyasingh on 5/29/18.
 */
public class Contact {
    private String name, phoneNumber, title, email, ID;

//    public Contact(){
//
//    }

    public Contact(String name, String phoneNumber, String title, String email, String ID){
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.title = title;
        this.email = email;
        this.ID = ID;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setEmail (String email) { this.email = email;}

    public String getEmail () {return email;}

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }
}

