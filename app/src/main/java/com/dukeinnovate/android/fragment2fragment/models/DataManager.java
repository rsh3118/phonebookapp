package com.dukeinnovate.android.fragment2fragment.models;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import com.dukeinnovate.android.fragment2fragment.JSONParserr;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import io.realm.MutableRealmInteger;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmObject;

import static android.content.ContentValues.TAG;

/**
 * Created by shreyasingh on 6/4/18.
 */

public class DataManager {


    private static DataManager sDataManager;
    private Realm mRealm;

    //Context context;

    private List<RealmContact> contacts;
    private List<RealmContact> list;


    DataManager(Context context) {

//        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder().build();
//        Realm.setDefaultConfiguration(realmConfiguration);

    }

    public List<RealmContact> getContacts() {
        return contacts;
    }

    public RealmContact getRealmContact(UUID id) {
        for (RealmContact realmContact : contacts) {
            if (realmContact.getId().equals(id)) {
                return realmContact;
            }
        }
        return null;
    }

    public static DataManager get(Context context){
        if (sDataManager == null){
            sDataManager = new DataManager(context);
        }
        return sDataManager;
    }


    public void loadJSON(Context context) {



            InputStream inputStream;
            try {
                Log.d(TAG, "inputstream: before reading json file");
                inputStream = context.getAssets().open("contactsample.json");
                Log.d(TAG, "inputstream: before creating gson");
                Gson gson = new GsonBuilder().create();
                Log.d(TAG, "inputstream to gson: gson builder is created");
                JsonElement json = new JsonParser().parse(new InputStreamReader(inputStream));
                Log.d(TAG, "inputstream: after parsing the json element");
                Log.d(TAG, "inputstream:" + json.toString());
                List<RealmContact> contacts = gson.fromJson(json,
                        new TypeToken<List<RealmContact>>() {

                        }.getType());
                Log.d(TAG, "inputstream: this is returning " + contacts.get(0).getName().toString() + " " +
                        contacts.get(0).getPhoneNumber().toString());
                mRealm = Realm.getDefaultInstance();
                mRealm.beginTransaction();
                Collection<RealmContact> realmContacts = mRealm.copyToRealmOrUpdate(contacts);


//            Set<String> seen = new HashSet<>();
//            Iterator<RealmContact> it = realmContacts.iterator();
//            while(it.hasNext()){
//                 RealmContact realmContact = it.next();
//                String id = realmContact.getId();
//                if (seen.contains(id)){
//                    it.remove();
//                }
//                seen.add(id);
//            }
                mRealm.commitTransaction();

                mRealm.close();



            } catch (IOException e) {

            }


        }





}
