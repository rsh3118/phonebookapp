package com.dukeinnovate.android.fragment2fragment.models;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dukeinnovate.android.fragment2fragment.Frag1;
import com.dukeinnovate.android.fragment2fragment.Fragment_2;
import com.dukeinnovate.android.fragment2fragment.OnContactSelectedListener;
import com.dukeinnovate.android.fragment2fragment.R;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;

import static android.content.ContentValues.TAG;

/**
 * Created by shreyasingh on 6/5/18.
 */

public class RealmContactsAdapter extends RealmRecyclerViewAdapter<RealmContact, RealmContactsAdapter.ViewHolder> implements Frag1.OnFragmentSendText{

    private Context context;
    OrderedRealmCollection<RealmContact> data;
    Realm mRealm;
    RealmContact mContact;
    String send_name, send_phoneNumber, send_title, send_email, send_ID;
    Frag1.OnFragmentSendText mSendText;
    RecyclerView mRecyclerView;
    private OnContactSelectedListener mOnContactSelectedListener;

    Fragment fragment;



    @Override
    public void onSentText(String text, String text2, String text3, String text4, String text5) {

        Bundle args = new Bundle();
        Log.d(TAG, "onSentText: " + text);
        args.putString("text", text);
        Log.d(TAG, "onSentText: " + text2);
        args.putString("text2",text2);
        Log.d(TAG, "onSentText: " + text3);
        args.putString("text3", text3);
        Log.d(TAG, "onSentText: " + text4);
        args.putString("text4", text4);
        Log.d(TAG, "onSentText: " + text5);
        args.putString("text5", text5);
        fragment.setArguments(args);
        Log.d(TAG, "onSentText:" + fragment);
        fragment.getFragmentManager().beginTransaction()
                .replace(R.id.container1, new Fragment_2(), "fragment2")
                .commit();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        private ConstraintLayout Rel1;
        private TextView name, title, phoneNumber, email, ID;

        private ConstraintLayout Rel2;
        private ImageView phoneIcon;

        public ViewHolder(View v){
            super(v);
            Rel1 = v.findViewById(R.id.rel_1);
            name = (TextView) v.findViewById(R.id.name);
            phoneNumber = (TextView) v.findViewById(R.id.phoneNumber);
            title = (TextView) v.findViewById(R.id.title);
            email = (TextView) v.findViewById(R.id.email_appearance);
            ID = (TextView) v.findViewById(R.id.ID_appearance);

            Rel2 = v.findViewById(R.id.rel_2);
            phoneIcon = v.findViewById(R.id.phone_icon);




        }

    }

    public RealmContactsAdapter(Context context, @NonNull  OrderedRealmCollection<RealmContact> data, boolean autoUpdate, Fragment fragment){
        super(data,true);
        this.context = context;
        this.data = data;
        this.fragment = fragment;
    }


    @NonNull
    @Override
    public RealmContactsAdapter.ViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, int viewType) {



        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_list_row, parent, false);

        ConstraintLayout rel2 = (ConstraintLayout)itemView.findViewById(R.id.rel_2);


        //phoneIcon.setClickable(true);

        return new RealmContactsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        RealmContact contact = data.get(position);
        holder.name.setText(contact.getName());
        holder.phoneNumber.setText(contact.getPhoneNumber());
        holder.title.setText(contact.getTitle());
        holder.email.setText(contact.getEmail());

        holder.Rel2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), "you wish to call", Toast.LENGTH_SHORT).show();
            }
        });

        final int contactPosition = holder.getLayoutPosition();


        holder.Rel1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //RealmResults<RealmContact> results = mRealm.where(RealmContact.class).findAll();

                Toast.makeText(v.getContext(), "you have selected" + contactPosition, Toast.LENGTH_SHORT).show();
                RealmContact contact = data.get(contactPosition);
                send_name = contact.getName();
                send_phoneNumber = contact.getPhoneNumber();
                send_title = contact.getTitle();
                send_email = contact.getEmail();
                send_ID = contact.getId();
                Log.d(TAG, "listener onClick: " + contact.getName());

                StaticVariable.onButtonPressed = true;


                onSentText(send_name,send_phoneNumber, send_title, send_email, send_ID);



            }
        });



    }

    public RealmResults<RealmContact> fetchContacts(Realm realm){
        Log.d(TAG, "method to fetch contacts: before fetching contacts");
        //realm = Realm.getDefaultInstance();
        RealmResults<RealmContact> results = realm.where(RealmContact.class).sort("name").findAll();
        //results = results.sort("name", Sort.ASCENDING);


        Log.d(TAG, "method to fetch contacts: " + results.get(0).toString());


        return results;

    }


    public void filterList(RealmResults<RealmContact> filteredList){
        data = filteredList;
        notifyDataSetChanged();
    }

//    @Override
//    public long getItemId(int position) {
//        return position;
//    }
//
//
//    @Override
//    public int getItemViewType(int position) {
//        return position;
//    }


    @Override
    public int getItemCount() {
        return data.size();
    }













}
