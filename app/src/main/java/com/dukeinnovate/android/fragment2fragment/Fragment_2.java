package com.dukeinnovate.android.fragment2fragment;

import android.Manifest;
import android.app.Fragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import static android.content.ContentValues.TAG;

/**
 * Created by shreyasingh on 5/27/18.
 */

public class Fragment_2 extends Fragment {

    TextView contactName, contactPhoneNumber, contactDescription, contactEmail, contactID;
    String stringText, stringText2, stringText3, stringText4, stringText5;
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_2, container, false);
        contactName = v.findViewById(R.id.name_content);
        contactPhoneNumber = v.findViewById(R.id.phone_number_content);
        contactDescription = v.findViewById(R.id.description_content);
        contactEmail = v.findViewById(R.id.email_content);
        contactID = v.findViewById(R.id.ID);

        contactPhoneNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "you wish to call the number: "
                        + contactPhoneNumber.getText().toString(), Toast.LENGTH_SHORT).show();
                phoneCall();

            }
        });

        contactEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "you wish to email: "
                + contactEmail.getText().toString(), Toast.LENGTH_SHORT).show();
                email();

            }
        });



        return v;
    }

    public void sentText(){
        new MyTask().execute();
    }

    public void setText(final String string, final String string2, final String string3, final String string4, final String string5){
        contactName.setText(string);
        contactPhoneNumber.setText(string2);
        contactDescription.setText(string3);
        contactEmail.setText(string4);
        contactID.setText(string5);
    }

    private void phoneCall() {
        //Intent callIntent = new Intent(Intent.ACTION_CALL);
        //callIntent.setData(Uri.parse("tel:" + "9193089611"));
        //startActivity(callIntent);

        String number = ("tel:" + contactPhoneNumber.getText());
        Intent mIntent = new Intent(Intent.ACTION_CALL);
        mIntent.setData(Uri.parse(number));

        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.CALL_PHONE},
                    MY_PERMISSIONS_REQUEST_CALL_PHONE);
        } else {
            try {
                startActivity(mIntent);
            } catch (SecurityException e){
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case MY_PERMISSIONS_REQUEST_CALL_PHONE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED){

                } else {

                }
                return;

            }
        }
    }

    private void email(){
        Log.i("send email", "");
        String[] TO = {contactEmail.getText().toString()};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        //emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Your subject");
        //emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message goes here");

        try{
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            Log.i("Finished sending email...", "");
        } catch (android.content.ActivityNotFoundException ex){
            Toast.makeText(getActivity(), "There is no email client installed ", Toast.LENGTH_SHORT).show();
        }

    }

    private class MyTask extends AsyncTask<String, String, String>{

        @Override
        protected String doInBackground(String... strings) {

            Bundle b = getArguments();
            Log.d(TAG, "doInBackground: " + stringText);
            stringText = b.getString("text");
            stringText2 = b.getString("text2");
            Log.d(TAG, "doInBackground: " + stringText2);
            stringText3 = b.getString("text3");
            Log.d(TAG, "doInBackground: " + stringText3);
            stringText4 = b.getString("text4");
            Log.d(TAG, "doInBackground: " + stringText4);
            stringText5 = b.getString("text5");
            Log.d(TAG, "doInBackground: " + stringText5);

            return null;
        }
//
        protected void onPostExecute(String result){
            setText(stringText, stringText2, stringText3, stringText4, stringText5);
        }




//
//        protected void onPostExecute2(String result){
//            setText(stringText2);
//
//        }
    }
}
