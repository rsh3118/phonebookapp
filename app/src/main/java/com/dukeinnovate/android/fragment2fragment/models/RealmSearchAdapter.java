package com.dukeinnovate.android.fragment2fragment.models;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dukeinnovate.android.fragment2fragment.ContactsAdapter;
import com.dukeinnovate.android.fragment2fragment.R;

import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.realm.Case;
import io.realm.OrderedRealmCollection;
import io.realm.Realm;

import io.realm.RealmObject;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;
import io.realm.Sort;

import static android.content.ContentValues.TAG;

/**
 * Created by shreyasingh on 6/7/18.
 */
public class RealmSearchAdapter extends RealmRecyclerViewAdapter<RealmContact, RealmSearchAdapter.MyyViewHolder> {

    private Context context;
    OrderedRealmCollection<RealmContact> data;
    String editTextString;

    public  RealmSearchAdapter(Context context, @NonNull OrderedRealmCollection<RealmContact> data, boolean autoUpdate, String editTextString) {
        super(data,true);
        this.context = context;
        this.data = data;
        this.editTextString = editTextString;
    }




    @NonNull
    @Override
    public MyyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_list_row, parent, false);

        return new RealmSearchAdapter.MyyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyyViewHolder holder, int position) {
        RealmContact contact = data.get(position);
        holder.name.setText(contact.getName());
        holder.phoneNumber.setText(contact.getPhoneNumber());
        holder.title.setText(contact.getTitle());
        holder.email.setText(contact.getEmail());

    }


    class MyyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, title, phoneNumber, email, ID;

        public MyyViewHolder(View v){
            super(v);
            name = (TextView) v.findViewById(R.id.name);
            phoneNumber = (TextView) v.findViewById(R.id.phoneNumber);
            title = (TextView) v.findViewById(R.id.title);
            email = (TextView) v.findViewById(R.id.email_appearance);
            ID = (TextView) v.findViewById(R.id.ID_appearance);
        }
    }

    public RealmResults<RealmContact> fetchFilteredContacts(Realm realm, String typed){

//        if (typed.equals("")){
//            results = realm.where(RealmContact.class).findAll();
//        } else {
            RealmResults<RealmContact> results = realm.where(RealmContact.class).beginsWith("name", typed, Case.INSENSITIVE ).findAll();
        //Log.d(TAG, "this is fetchFilteredContacts: " + results.get(0).getName() + " " + results.get(1).getName());

        //}
        notifyDataSetChanged();

        return results;

    }

    @Override
    public int getItemCount() {
        return data.size();
    }








}


//public abstract class RealmSearchAdapter extends RealmBasedRecyclerViewAdapter<>{
//
//    private Realm realm;
//    protected Class<RealmContact> clazz;
//
//    private String filterKey;
//
//    private boolean useContains;
//    private Case casing;
//    private Sort sortOrder;
//    private String sortKey;
//    private String basePredicate;
//
//    public RealmSearchAdapter(
//            @NonNull Context context,
//            @NonNull Realm realm,
//            @NonNull String filterKey) {
//        this(context, realm, filterKey, true, Case.INSENSITIVE, Sort.ASCENDING, filterKey, null);
//
//
//    }
//
//    @SuppressWarnings("unchecked")
//    public RealmSearchAdapter(
//            @NonNull Context context,
//            @NonNull Realm realm,
//            @NonNull String filterKey,
//            boolean useContains,
//            Case casing,
//            Sort sortOrder,
//            String sortKey,
//            String basePredicate) {
//        super(context, null, false, false);
//        this.realm = realm;
//        this.filterKey = filterKey;
//        this.useContains = useContains;
//        this.casing = casing;
//        this.sortOrder = sortOrder;
//        this.sortKey = sortKey;
//        this.basePredicate = basePredicate;
//
//        clazz = (Class<RealmContact>) getTypeArguements(RealmSearchAdapter.class, getClass()).get(0);
//    }
//
//    @Override
//    public void onBindFooterViewHolder(VH holder, int position) {
//        holder.footerTextView.setText("I'm a footer");
//    }
//
//    public static Class<?> getClass(Type type){
//        if (type instanceof Class){
//            return (Class) type;
//        } else if (type instanceof ParameterizedType){
//            return getClass(((ParameterizedType) ((ParameterizedType) type).getRawType()));
//        } else if (type instanceof GenericArrayType){
//            Type componentType = ((GenericArrayType) type).getGenericComponentType();
//            Class<?> componentClass = getClass(componentType);
//            if (componentClass != null) {
//                return Array.newInstance(componentClass, 0).getClass();
//
//            } else {
//                return null;
//            }
//        } else {
//            return null;
//        }
//    }
//
//    public static <RealmContact>List<Class<?>> getTypeArguements(Class<RealmContact> baseClass, Class<? extends RealmContact> childclass){
//
//        Map<Type, Type> resolvedTypes = new HashMap<Type, Type>();
//        Type type = childclass;
//
//        while (!getClass(type).equals(baseClass)){
//            if (type instanceof Class){
//                type = ((Class) type).getGenericSuperclass();
//            } else {
//                ParameterizedType parameterizedType = (ParameterizedType) type;
//                Class<?> rawType = (Class) parameterizedType.getRawType();
//
//                Type[] actualTypeArguements = parameterizedType.getActualTypeArguments();
//                TypeVariable<?>[] typeParameters = rawType.getTypeParameters();
//                for (int i = 0; i <actualTypeArguements.length; i++){
//                    resolvedTypes.put(typeParameters[i], actualTypeArguements[i]);
//                }
//                if (!rawType.equals(baseClass)){
//                    type = rawType.getGenericSuperclass();
//                }
//            }
//        }
//
//        Type[] actualTypeArguements;
//        if (type instanceof Class){
//            actualTypeArguements = ((Class) type).getTypeParameters();
//        } else {
//            actualTypeArguements = ((ParameterizedType) type).getActualTypeArguments();
//        }
//        List<Class<?>> typeArguementsAsClasses = new ArrayList<Class<?>>();
//        for (Type baseType : actualTypeArguements){
//            while (resolvedTypes.containsKey(baseType)) {
//                baseType = resolvedTypes.get(baseType);
//
//            }
//            typeArguementsAsClasses.add(getClass(baseType));
//        }
//        return typeArguementsAsClasses;
//
//    }



