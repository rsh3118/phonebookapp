package com.dukeinnovate.android.fragment2fragment;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements Frag1.OnFragmentSendText {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            if (savedInstanceState == null){
                getFragmentManager().beginTransaction().add(R.id.container1, new Frag1())
                        //.add(R.id.container1, new Fragment_1())
                        .commit();
            }
        } else {
            if (savedInstanceState == null){
                getFragmentManager().beginTransaction()
                        .add(R.id.container1, new Fragment_2(), "fragment2")
                        .commit();
            }
        }
    }

    @Override
    public void onSentText(String text, String text2, String text3, String text4, String text5) {

        Fragment_2 fragment_2 = (Fragment_2)getFragmentManager().findFragmentByTag("fragment2");
        if (fragment_2 != null){
            fragment_2.setText(text, text2, text3, text4, text5);
        } else {
            Fragment_2 fragment = new Fragment_2();
            Bundle args = new Bundle();
            args.putString("text", text);
            args.putString("text2",text2);
            args.putString("text3", text3);
            args.putString("text4", text4);
            args.putString("text5", text5);
            fragment.setArguments(args);
            getFragmentManager().beginTransaction()
                    .replace(R.id.container1, fragment)
                    .addToBackStack(null).commit();
            fragment.sentText();
        }

    }



//    @Override
//    public void onSentText(String text) {
//        Fragment_2 fragment_2 = (Fragment_2)getFragmentManager().findFragmentByTag("fragment2");
//        if (fragment_2 != null){
//            fragment_2.setText(text);
//        } else {
//            Fragment_2 fragment = new Fragment_2();
//            Bundle args = new Bundle();
//            args.putString("text", text);
//            fragment.setArguments(args);
//            getFragmentManager().beginTransaction()
//                    .replace(R.id.container1, fragment)
//                    .addToBackStack(null).commit();
//            fragment.sentText();
//        }
//
//    }
}
