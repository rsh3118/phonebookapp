package com.dukeinnovate.android.fragment2fragment;

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shreyasingh on 5/29/18.
 */

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.MyViewHolder> implements Frag1.OnFragmentSendText{

    private List<Contact> contactList;


    @Override
    public void onSentText(String text, String text2, String text3, String text4, String text5) {

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, title, phoneNumber, email, ID;
        private ConstraintLayout rel1;
        private ConstraintLayout rel2;
        public MyViewHolder (View view) {
            super(view);
            rel1 =view.findViewById(R.id.rel_1);
            rel2 = view.findViewById(R.id.rel_2);
            name = (TextView) view.findViewById(R.id.name);
            phoneNumber = (TextView) view.findViewById(R.id.phoneNumber);
            title = (TextView) view.findViewById(R.id.title);
            email = (TextView) view.findViewById(R.id.email_appearance);
            ID = (TextView) view.findViewById(R.id.ID_appearance);






        }
    }

    public ContactsAdapter(List<Contact> contactList){
        this.contactList = contactList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_list_row, parent, false);



        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Contact contact = contactList.get(position);
        holder.name.setText(contact.getName());
        holder.phoneNumber.setText(contact.getPhoneNumber());
        holder.title.setText(contact.getTitle());
        holder.email.setText(contact.getEmail());
        holder.ID.setText(contact.getID());

//        RealmContact contact = contactsFromRealm.get(position);
//        //phoneIcon = view.findViewById(R.id.phone_icon);
//
//
////                phoneIcon.setOnClickListener(new View.OnClickListener() {
////                    @Override
////                    public void onClick(View v) {
////                        Toast.makeText(getContext(), "you want to call", Toast.LENGTH_SHORT).show();
////                        StaticVariable.onButtonPressed = true;
////                        Log.d(TAG, "onClick: " + StaticVariable.onButtonPressed);
////                    }
////                });
//        Toast.makeText(getContext(), contact.getName() + " is selected!", Toast.LENGTH_SHORT).show();
//        send_name = contact.getName();
//        send_phoneNumber = contact.getPhoneNumber();
//        send_title = contact.getTitle();
//        send_email = contact.getEmail();
//        send_ID = contact.getId();
//        //if (!StaticVariable.onButtonPressed) {
//
//        mSendText.onSentText(send_name, send_phoneNumber, send_title, send_email, send_ID);
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    public void filterList(ArrayList<Contact> filteredList){
        contactList = filteredList;
        notifyDataSetChanged();
    }



}



