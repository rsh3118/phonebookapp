package com.dukeinnovate.android.fragment2fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import com.dukeinnovate.android.fragment2fragment.models.DataManager;
import com.dukeinnovate.android.fragment2fragment.models.RealmContact;
import com.dukeinnovate.android.fragment2fragment.models.RealmContactsAdapter;
import com.dukeinnovate.android.fragment2fragment.models.RealmSearchAdapter;

import java.util.ArrayList;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

import static android.content.ContentValues.TAG;

/**
 * Created by shreyasingh on 5/27/18.
 */

public class Frag1 extends Fragment {

    OnFragmentSendText mSendText;
    String send_name, send_phoneNumber, send_title, send_email, send_ID;
    DataManager dataManager;
    private ImageView phoneIcon;
    private int mPosition;
    private RealmList<RealmContact> mList;



    EditText text;

    private ArrayList<Contact> contactList = new ArrayList<>();
    private ArrayList<Contact> filteredList = new ArrayList<>();
    RealmResults<RealmContact> contactsFromRealm;
    RealmResults<RealmContact> filteredContactsFromRealm;
    private RecyclerView recyclerView;
    RealmContactsAdapter adapter;
    RealmContactsAdapter adapter1;
    RealmSearchAdapter realmSearchAdapter;
    Context context;
//    private ContactsAdapter mAdapter;
//    private ContactsAdapter mAdapter1;
    String k;

    private Realm realm;



    public interface OnFragmentSendText{
        void onSentText(String text, String text2, String text3, String text4, String text5);
    }

    public Frag1(){

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mSendText = (OnFragmentSendText)context;
        } catch (ClassCastException e){
            throw new ClassCastException(getActivity().toString() + "must implement OnFragmentSendText");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, Bundle savedInstanceState) {




        View v = inflater.inflate(R.layout.fragment_one, container, false);

        realm = Realm.getDefaultInstance();
        contactsFromRealm = realm.where(RealmContact.class).sort("name").findAll();

        adapter = new RealmContactsAdapter(getContext(), contactsFromRealm, false, this);



        //realmSearchAdapter = new RealmSearchAdapter(getContext(), filteredContactsFromRealm, false,k );
        //contactsFromRealm = adapter.fetchContacts(realm);
        Log.d(TAG, "Here is what Realm is getting CreateView: " + contactsFromRealm.get(0).getName());

        //adapter = new RealmContactsAdapter(getContext(), contactsFromRealm,true);
        //adapter1 = new RealmContactsAdapter(getContext(),contactsFromRealm, true);
        //adapter = adapter1;



        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);








        text = (EditText) v.findViewById(R.id.input);
        final boolean onPhoneIconPressed = false;

//        if (!StaticVariable.onButtonPressed) {
//
//        }
//        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getContext(),
//                recyclerView, new RecyclerTouchListener.ClickListener() {
//            @Override
//            public void onClick(View view, int position) {
//
//                RealmContact contact = contactsFromRealm.get(position);
//                //phoneIcon = view.findViewById(R.id.phone_icon);
//
//
////                phoneIcon.setOnClickListener(new View.OnClickListener() {
////                    @Override
////                    public void onClick(View v) {
////                        Toast.makeText(getContext(), "you want to call", Toast.LENGTH_SHORT).show();
////                        StaticVariable.onButtonPressed = true;
////                        Log.d(TAG, "onClick: " + StaticVariable.onButtonPressed);
////                    }
////                });
//                Toast.makeText(getContext(), contact.getName() + " is selected!", Toast.LENGTH_SHORT).show();
//                send_name = contact.getName();
//                send_phoneNumber = contact.getPhoneNumber();
//                send_title = contact.getTitle();
//                send_email = contact.getEmail();
//                send_ID = contact.getId();
//                //if (!StaticVariable.onButtonPressed) {
//
//                mSendText.onSentText(send_name, send_phoneNumber, send_title, send_email, send_ID);
//
//                //}
//
//
//
//            }
//            //
//            @Override
//            public void onLongClick(View view, int position) {
//
//            }
//        }));

        text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().equals(null)){
                    hideSoftKeyBoard();
                }
                realm = Realm.getDefaultInstance();
                //Toast.makeText(getContext(), "You have entered " + s.toString(), Toast.LENGTH_SHORT).show();

                filteredContactsFromRealm = realm.where(RealmContact.class).beginsWith("name", s.toString(), Case.INSENSITIVE ).findAll();
                realmSearchAdapter = new RealmSearchAdapter(getContext(), filteredContactsFromRealm, true, s.toString());
                recyclerView.setAdapter(realmSearchAdapter);
                realmSearchAdapter.notifyDataSetChanged();

                //filter(s.toString());



//                filteredContactsFromRealm = realmSearchAdapter.fetchFilteredContacts(realm, s.toString());
//                Log.d(TAG, "this is afterTextChanged: " + filteredContactsFromRealm.toString());
//                realmSearchAdapter = new RealmSearchAdapter(getContext(), filteredContactsFromRealm, true, s.toString());
//                //recyclerView.setLayoutManager(mLayoutManager);
//                recyclerView.setAdapter(realmSearchAdapter);
                //adapter.notifyDataSetChanged();

//                adapter1 = new RealmContactsAdapter(getContext(), filteredContactsFromRealm, true);
//                filteredContactsFromRealm = adapter.filteredContacts(realm,s.toString());
//                Log.d(TAG, "this is afterTextChanged: " + filteredContactsFromRealm.get(0).getName());
//
//                recyclerView.setAdapter(adapter1);
//                adapter1.notifyDataSetChanged();

         }
        });

        return  v;


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //recyclerView.setAdapter(null);
        realm.close();
    }

    private void filter(String text){
        //filteredContactsFromRealm = realmSearchAdapter.fetchFilteredContacts(realm, text);
        filteredContactsFromRealm = realm.where(RealmContact.class).beginsWith("name", text).findAll();
        Log.d(TAG, "this is afterTextChanged: " + filteredContactsFromRealm.toString());
        realmSearchAdapter = new RealmSearchAdapter(getContext(), filteredContactsFromRealm, true, text);
        //recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(realmSearchAdapter);
        realmSearchAdapter.notifyDataSetChanged();
    }

//    @Override
//    public void onStop() {
//        super.onStop();
//        realm.close();
//    }

    //    private RealmResults<RealmContact> filter(String text){
//        for (RealmContact contact : contactsFromRealm) {
//            if (contact.getName().toLowerCase().contains(text.toLowerCase())) {
//                filteredContactsFromRealm.add(contact);
//            }
//        }
//        adapter.filterList(filteredContactsFromRealm);
//        return filteredContactsFromRealm;
//    }

//    public void filter(String text){
//        filteredContactsFromRealm = adapter.filteredContacts(realm,text);
//        adapter = new RealmContactsAdapter(getContext(), filteredContactsFromRealm, true);
//        recyclerView.setAdapter(adapter);
//        adapter.notifyDataSetChanged();
//
//
//
//    }



//    private ArrayList<Contact> filter(String text){
//        ArrayList<Contact> filteredListt = new ArrayList<>();
//        for (Contact contact : contactList) {
//            if (contact.getName().toLowerCase().contains(text.toLowerCase())) {
//                filteredListt.add(contact);
//            }
//        }
//        mAdapter.filterList(filteredListt);
//        return filteredListt;
//    }

    public void hideSoftKeyBoard(){
        InputMethodManager inputManager =
                (InputMethodManager) getActivity().
                        getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(
                getActivity().getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);

//        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
//        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }




}
