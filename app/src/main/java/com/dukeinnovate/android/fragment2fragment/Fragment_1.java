package com.dukeinnovate.android.fragment2fragment;

import android.app.Fragment;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.UUID;

import io.realm.Realm;

/**
 * Created by shreyasingh on 5/27/18.
 */

public class Fragment_1 extends Fragment {

//    OnFragmentSendText mSendText;
//    String send_name, send_phoneNumber, send_title, send_email, send_ID;
//
//
//    EditText text;
//
//    private ArrayList<Contact> contactList = new ArrayList<>();
//    private ArrayList<Contact> filteredList = new ArrayList<>();
//    private RecyclerView recyclerView;
//    private ContactsAdapter mAdapter;
//    private ContactsAdapter mAdapter1;
//
//
//    private Realm realm;
//
//
//    public interface OnFragmentSendText{
//        public void onSentText(String text, String text2, String text3, String text4, String text5);
//    }
//
//    public Fragment_1(){
//
//    }
//
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        try {
//            mSendText = (OnFragmentSendText)context;
//        } catch (ClassCastException e){
//            throw new ClassCastException(getActivity().toString() + "must implement OnFragmentSendText");
//        }
//    }
//
////    @Nullable
////    @Override
////    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, Bundle savedInstanceState) {
//////        View v = inflater.inflate(R.layout.fragment_one, container, false);
//////
////////        realm = Realm.getDefaultInstance();
////////        RealmResults<RealmContact> contactsFromRealm = realm.where(RealmContact.class).findAll();
//////
//////        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
//////        text = (EditText) v.findViewById(R.id.input);
//////
//////        mAdapter = new ContactsAdapter(contactList);
//////        //mAdapter = new ContactsAdapter(filteredList);
//////        //mAdapter1 = new MoviesAdapter(filteredList);
//////        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
//////        recyclerView.setLayoutManager(mLayoutManager);
//////        recyclerView.setItemAnimator(new DefaultItemAnimator());
//////        recyclerView.setAdapter(mAdapter);
//////        //recyclerView.setAdapter(mAdapter1);
//////        //realm = Realm.getDefaultInstance();
//////
//////
//////
//////        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getContext(),
//////                recyclerView, new RecyclerTouchListener.ClickListener() {
//////            @Override
//////            public void onClick(View view, int position) {
//////                //ArrayList<Movie> dummyList = movieList;
//////                //Movie movie = dummyList.get(position);
//////                filteredList = contactList;
//////                Contact contact = filteredList.get(position);
//////                Toast.makeText(getContext(), contact.getName() + " is selected!", Toast.LENGTH_SHORT).show();
//////                send_name = contact.getName();
//////                send_phoneNumber = contact.getPhoneNumber();
//////                send_title = contact.getTitle();
//////                send_email = contact.getEmail();
//////                send_ID = contact.getID();
//////                mSendText.onSentText(send_name, send_phoneNumber, send_title, send_email, send_ID);
//////            }
//////            //
//////            @Override
//////            public void onLongClick(View view, int position) {
//////
//////            }
//////        }));
//////
//////        text.addTextChangedListener(new TextWatcher() {
//////            @Override
//////            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//////            }
//////
//////            @Override
//////            public void onTextChanged(CharSequence s, int start, int before, int count) {
//////            }
//////
//////            @Override
//////            public void afterTextChanged(Editable s) {
//////                filteredList = filter(s.toString());
//////                //movieList = filteredList;
//////            }
//////        });
//////
//////
//////        //prepareMovieData(mLayoutManager);     //uses the values given by the JSON file
//////        prepareDataWithJson(mLayoutManager);   //uses the hard coded values
////        //return  v;
////
////
////
////
////
////    }
//
//    private ArrayList<Contact> filter(String text){
//        ArrayList<Contact> filteredListt = new ArrayList<>();
//            for (Contact contact : contactList) {
//                if (contact.getName().toLowerCase().contains(text.toLowerCase())) {
//                    filteredListt.add(contact);
//                }
//            }
//            mAdapter.filterList(filteredListt);
//            return filteredListt;
//    }
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        recyclerView.setAdapter(null);
//        realm.close();
//    }
//
//    private void prepareDataWithJson(RecyclerView.LayoutManager mLayout){
//        Resources res = getResources();
//        String JObject = res.getString(R.string.jsonContact);
//        String JObject1 = res.getString(R.string.jsonContact1);
//
//        Contact contact1 = JSONParserr.parse(JObject);
//        contactList.add(contact1);
//
//        Contact contact2 = JSONParserr.parse(JObject1);
//
//        contactList.add(contact2);
//
//        Contact contact = new Contact("Pizza Hut", "9193089611", "Doctor - Burn Center", "ss721@duke.edu", UUID.randomUUID().toString());
//        contactList.add(contact);
//
//        contact = new Contact("Subway", "9193089611", "Nurse - Endoscopy Unit", "ss721@duke.edu", UUID.randomUUID().toString());
//        contactList.add(contact);
//
//        contact = new Contact("Heavenly Buffaloes", "9193089611", "Nurse - Neuroscience", "ss721@duke.edu", UUID.randomUUID().toString());
//        contactList.add(contact);
//
//        contact = new Contact("Dale's", "9193089611", "Nurse - Neuroscience", "ss721@duke.edu", UUID.randomUUID().toString());
//        contactList.add(contact);
//
//        contact = new Contact("Sake Bomb", "9193089611", "Nurse - Endoscopy Unit", "ss721@duke.edu", UUID.randomUUID().toString());
//        contactList.add(contact);
//
//        contact = new Contact("Pad Thai", "9193089611", "Nurse - Neuroscience", "ss721@duke.edu", UUID.randomUUID().toString());
//        contactList.add(contact);
//
//        contact = new Contact("Cary", "9193089611", "Nurse - Neuroscience", "ss721@duke.edu", UUID.randomUUID().toString());
//        contactList.add(contact);
//
//        contact = new Contact("Jonas Doe", "9193089611", "Nurse - Neuroscience", "ss721@duke.edu", UUID.randomUUID().toString());
//        contactList.add(contact);
//
//
////        Log.d(TAG, "parsed onCreateView: " + contact1.getName() + " " + contact1.getPhoneNumber() + " "
////                + contact1.getTitle() + " " + contact1.getEmail());
//
//        mAdapter.notifyDataSetChanged();
//
//
//    }
//
//    private void prepareMovieData(RecyclerView.LayoutManager mLayoutPassin){
//        Contact contact = new Contact("John Doe", "9193089611", "Nurse - Neuroscience", "ss721@duke.edu", UUID.randomUUID().toString());
//        contactList.add(contact);
//
//        contact = new Contact("Jane Doe", "9197487021", "Doctor - Cardiology", "ss721@duke.edu", UUID.randomUUID().toString());
//        contactList.add(contact);
//
//        contact = new Contact("Dominos", "2096693131", "Nurse - Dermatology", "ss721@duke.edu", UUID.randomUUID().toString());
//        contactList.add(contact);
//
//        contact = new Contact("Jimmy Johns", "9193089611", "OnCall - Radiology", "ss721@duke.edu", UUID.randomUUID().toString());
//        contactList.add(contact);
//
//        contact = new Contact("Elmo's", "9193089611", "Doctor - Pediatric", "ss721@duke.edu", UUID.randomUUID().toString());
//        contactList.add(contact);
//
//        contact = new Contact("Pizza Hut", "9193089611", "Doctor - Burn Center", "ss721@duke.edu", UUID.randomUUID().toString());
//        contactList.add(contact);
//
//        contact = new Contact("Subway", "9193089611", "Nurse - Endoscopy Unit", "ss721@duke.edu", UUID.randomUUID().toString());
//        contactList.add(contact);
//
//        contact = new Contact("Heavenly Buffaloes", "9193089611", "Nurse - Neuroscience", "ss721@duke.edu", UUID.randomUUID().toString());
//        contactList.add(contact);
//
//        contact = new Contact("Dale's", "9193089611", "Nurse - Neuroscience", "ss721@duke.edu", UUID.randomUUID().toString());
//        contactList.add(contact);
//
//        contact = new Contact("Moe's", "9193089611", "Nurse - Cancer", "ss721@duke.edu", UUID.randomUUID().toString());
//        contactList.add(contact);
//
//        contact = new Contact("Jill", "9193089611", "Nurse - Pharmacy", "ss721@duke.edu", UUID.randomUUID().toString());
//        contactList.add(contact);
//
//        contact = new Contact("Karen Smith", "9193089611", "Doctor - Burn Center", "ss721@duke.edu", UUID.randomUUID().toString());
//        contactList.add(contact);
//
//        contact = new Contact("Sake Bomb", "9193089611", "Nurse - Endoscopy Unit", "ss721@duke.edu", UUID.randomUUID().toString());
//        contactList.add(contact);
//
//        contact = new Contact("Pad Thai", "9193089611", "Nurse - Neuroscience", "ss721@duke.edu", UUID.randomUUID().toString());
//        contactList.add(contact);
//
//        contact = new Contact("Cary", "9193089611", "Nurse - Neuroscience", "ss721@duke.edu", UUID.randomUUID().toString());
//        contactList.add(contact);
//
//        contact = new Contact("Jonas Doe", "9193089611", "Nurse - Neuroscience", "ss721@duke.edu", UUID.randomUUID().toString());
//        contactList.add(contact);
//
//
//
//        mAdapter.notifyDataSetChanged();
//    }
}
