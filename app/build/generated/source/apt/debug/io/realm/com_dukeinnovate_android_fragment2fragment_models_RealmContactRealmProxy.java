package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.ProxyUtils;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxy extends com.dukeinnovate.android.fragment2fragment.models.RealmContact
    implements RealmObjectProxy, com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface {

    static final class RealmContactColumnInfo extends ColumnInfo {
        long nameIndex;
        long idIndex;
        long phoneNumberIndex;
        long titleIndex;
        long emailIndex;
        long integerIndex;

        RealmContactColumnInfo(OsSchemaInfo schemaInfo) {
            super(6);
            OsObjectSchemaInfo objectSchemaInfo = schemaInfo.getObjectSchemaInfo("RealmContact");
            this.nameIndex = addColumnDetails("name", "name", objectSchemaInfo);
            this.idIndex = addColumnDetails("id", "id", objectSchemaInfo);
            this.phoneNumberIndex = addColumnDetails("phoneNumber", "phoneNumber", objectSchemaInfo);
            this.titleIndex = addColumnDetails("title", "title", objectSchemaInfo);
            this.emailIndex = addColumnDetails("email", "email", objectSchemaInfo);
            this.integerIndex = addColumnDetails("integer", "integer", objectSchemaInfo);
        }

        RealmContactColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new RealmContactColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final RealmContactColumnInfo src = (RealmContactColumnInfo) rawSrc;
            final RealmContactColumnInfo dst = (RealmContactColumnInfo) rawDst;
            dst.nameIndex = src.nameIndex;
            dst.idIndex = src.idIndex;
            dst.phoneNumberIndex = src.phoneNumberIndex;
            dst.titleIndex = src.titleIndex;
            dst.emailIndex = src.emailIndex;
            dst.integerIndex = src.integerIndex;
        }
    }

    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();

    private RealmContactColumnInfo columnInfo;
    private ProxyState<com.dukeinnovate.android.fragment2fragment.models.RealmContact> proxyState;
    private final MutableRealmInteger.Managed integerMutableRealmInteger = new MutableRealmInteger.Managed<com.dukeinnovate.android.fragment2fragment.models.RealmContact>() {
                @Override protected ProxyState<com.dukeinnovate.android.fragment2fragment.models.RealmContact> getProxyState() { return proxyState; }
                @Override protected long getColumnIndex() { return columnInfo.integerIndex; }
            };

    com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (RealmContactColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<com.dukeinnovate.android.fragment2fragment.models.RealmContact>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$name() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.nameIndex);
    }

    @Override
    public void realmSet$name(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'name' to null.");
            }
            row.getTable().setString(columnInfo.nameIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'name' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.nameIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$id() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.idIndex);
    }

    @Override
    public void realmSet$id(String value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'id' cannot be changed after object was created.");
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$phoneNumber() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.phoneNumberIndex);
    }

    @Override
    public void realmSet$phoneNumber(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'phoneNumber' to null.");
            }
            row.getTable().setString(columnInfo.phoneNumberIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'phoneNumber' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.phoneNumberIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$title() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.titleIndex);
    }

    @Override
    public void realmSet$title(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'title' to null.");
            }
            row.getTable().setString(columnInfo.titleIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'title' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.titleIndex, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$email() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.emailIndex);
    }

    @Override
    public void realmSet$email(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'email' to null.");
            }
            row.getTable().setString(columnInfo.emailIndex, row.getIndex(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'email' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.emailIndex, value);
    }

    @Override
    public MutableRealmInteger realmGet$integer() {
        proxyState.getRealm$realm().checkIfValid();
        return this.integerMutableRealmInteger;
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("RealmContact", 6, 0);
        builder.addPersistedProperty("name", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("id", RealmFieldType.STRING, Property.PRIMARY_KEY, Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("phoneNumber", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("title", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("email", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("integer", RealmFieldType.INTEGER, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static RealmContactColumnInfo createColumnInfo(OsSchemaInfo schemaInfo) {
        return new RealmContactColumnInfo(schemaInfo);
    }

    public static String getSimpleClassName() {
        return "RealmContact";
    }

    public static final class ClassNameHelper {
        public static final String INTERNAL_CLASS_NAME = "RealmContact";
    }

    @SuppressWarnings("cast")
    public static com.dukeinnovate.android.fragment2fragment.models.RealmContact createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        com.dukeinnovate.android.fragment2fragment.models.RealmContact obj = null;
        if (update) {
            Table table = realm.getTable(com.dukeinnovate.android.fragment2fragment.models.RealmContact.class);
            RealmContactColumnInfo columnInfo = (RealmContactColumnInfo) realm.getSchema().getColumnInfo(com.dukeinnovate.android.fragment2fragment.models.RealmContact.class);
            long pkColumnIndex = columnInfo.idIndex;
            long rowIndex = Table.NO_MATCH;
            if (!json.isNull("id")) {
                rowIndex = table.findFirstString(pkColumnIndex, json.getString("id"));
            }
            if (rowIndex != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.getSchema().getColumnInfo(com.dukeinnovate.android.fragment2fragment.models.RealmContact.class), false, Collections.<String> emptyList());
                    obj = new io.realm.com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("id")) {
                if (json.isNull("id")) {
                    obj = (io.realm.com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxy) realm.createObjectInternal(com.dukeinnovate.android.fragment2fragment.models.RealmContact.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxy) realm.createObjectInternal(com.dukeinnovate.android.fragment2fragment.models.RealmContact.class, json.getString("id"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'id'.");
            }
        }

        final com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface objProxy = (com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface) obj;
        if (json.has("name")) {
            if (json.isNull("name")) {
                objProxy.realmSet$name(null);
            } else {
                objProxy.realmSet$name((String) json.getString("name"));
            }
        }
        if (json.has("phoneNumber")) {
            if (json.isNull("phoneNumber")) {
                objProxy.realmSet$phoneNumber(null);
            } else {
                objProxy.realmSet$phoneNumber((String) json.getString("phoneNumber"));
            }
        }
        if (json.has("title")) {
            if (json.isNull("title")) {
                objProxy.realmSet$title(null);
            } else {
                objProxy.realmSet$title((String) json.getString("title"));
            }
        }
        if (json.has("email")) {
            if (json.isNull("email")) {
                objProxy.realmSet$email(null);
            } else {
                objProxy.realmSet$email((String) json.getString("email"));
            }
        }
        if (json.has("integer")) {
            objProxy.realmGet$integer().set((json.isNull("integer")) ? null : json.getLong("integer"));
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static com.dukeinnovate.android.fragment2fragment.models.RealmContact createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        final com.dukeinnovate.android.fragment2fragment.models.RealmContact obj = new com.dukeinnovate.android.fragment2fragment.models.RealmContact();
        final com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface objProxy = (com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface) obj;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("name")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$name((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$name(null);
                }
            } else if (name.equals("id")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$id((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$id(null);
                }
                jsonHasPrimaryKey = true;
            } else if (name.equals("phoneNumber")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$phoneNumber((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$phoneNumber(null);
                }
            } else if (name.equals("title")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$title((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$title(null);
                }
            } else if (name.equals("email")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$email((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$email(null);
                }
            } else if (name.equals("integer")) {
                Long val = null;
                if (reader.peek() != JsonToken.NULL) {
                    val = reader.nextLong();
                } else {
                    reader.skipValue();
                }
                objProxy.realmGet$integer().set(val);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'id'.");
        }
        return realm.copyToRealm(obj);
    }

    public static com.dukeinnovate.android.fragment2fragment.models.RealmContact copyOrUpdate(Realm realm, com.dukeinnovate.android.fragment2fragment.models.RealmContact object, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null) {
            final BaseRealm otherRealm = ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm();
            if (otherRealm.threadId != realm.threadId) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            }
            if (otherRealm.getPath().equals(realm.getPath())) {
                return object;
            }
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.dukeinnovate.android.fragment2fragment.models.RealmContact) cachedRealmObject;
        }

        com.dukeinnovate.android.fragment2fragment.models.RealmContact realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(com.dukeinnovate.android.fragment2fragment.models.RealmContact.class);
            RealmContactColumnInfo columnInfo = (RealmContactColumnInfo) realm.getSchema().getColumnInfo(com.dukeinnovate.android.fragment2fragment.models.RealmContact.class);
            long pkColumnIndex = columnInfo.idIndex;
            long rowIndex = table.findFirstString(pkColumnIndex, ((com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface) object).realmGet$id());
            if (rowIndex == Table.NO_MATCH) {
                canUpdate = false;
            } else {
                try {
                    objectContext.set(realm, table.getUncheckedRow(rowIndex), realm.getSchema().getColumnInfo(com.dukeinnovate.android.fragment2fragment.models.RealmContact.class), false, Collections.<String> emptyList());
                    realmObject = new io.realm.com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxy();
                    cache.put(object, (RealmObjectProxy) realmObject);
                } finally {
                    objectContext.clear();
                }
            }
        }

        return (canUpdate) ? update(realm, realmObject, object, cache) : copy(realm, object, update, cache);
    }

    public static com.dukeinnovate.android.fragment2fragment.models.RealmContact copy(Realm realm, com.dukeinnovate.android.fragment2fragment.models.RealmContact newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.dukeinnovate.android.fragment2fragment.models.RealmContact) cachedRealmObject;
        }

        // rejecting default values to avoid creating unexpected objects from RealmModel/RealmList fields.
        com.dukeinnovate.android.fragment2fragment.models.RealmContact realmObject = realm.createObjectInternal(com.dukeinnovate.android.fragment2fragment.models.RealmContact.class, ((com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface) newObject).realmGet$id(), false, Collections.<String>emptyList());
        cache.put(newObject, (RealmObjectProxy) realmObject);

        com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface realmObjectSource = (com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface) newObject;
        com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface realmObjectCopy = (com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface) realmObject;

        realmObjectCopy.realmSet$name(realmObjectSource.realmGet$name());
        realmObjectCopy.realmSet$phoneNumber(realmObjectSource.realmGet$phoneNumber());
        realmObjectCopy.realmSet$title(realmObjectSource.realmGet$title());
        realmObjectCopy.realmSet$email(realmObjectSource.realmGet$email());

        realmObjectCopy.realmGet$integer().set(realmObjectSource.realmGet$integer().get());
        return realmObject;
    }

    public static long insert(Realm realm, com.dukeinnovate.android.fragment2fragment.models.RealmContact object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.dukeinnovate.android.fragment2fragment.models.RealmContact.class);
        long tableNativePtr = table.getNativePtr();
        RealmContactColumnInfo columnInfo = (RealmContactColumnInfo) realm.getSchema().getColumnInfo(com.dukeinnovate.android.fragment2fragment.models.RealmContact.class);
        long pkColumnIndex = columnInfo.idIndex;
        long rowIndex = Table.NO_MATCH;
        Object primaryKeyValue = ((com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface) object).realmGet$id();
        if (primaryKeyValue != null) {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, (String)primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$name = ((com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface) object).realmGet$name();
        if (realmGet$name != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.nameIndex, rowIndex, realmGet$name, false);
        }
        String realmGet$phoneNumber = ((com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface) object).realmGet$phoneNumber();
        if (realmGet$phoneNumber != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.phoneNumberIndex, rowIndex, realmGet$phoneNumber, false);
        }
        String realmGet$title = ((com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface) object).realmGet$title();
        if (realmGet$title != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.titleIndex, rowIndex, realmGet$title, false);
        }
        String realmGet$email = ((com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface) object).realmGet$email();
        if (realmGet$email != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.emailIndex, rowIndex, realmGet$email, false);
        }
        Long realmGet$integer = ((com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface) object).realmGet$integer().get();
        if (realmGet$integer != null) {
            Table.nativeSetLong(tableNativePtr, columnInfo.integerIndex, rowIndex, realmGet$integer.longValue(), false);
        }
        return rowIndex;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.dukeinnovate.android.fragment2fragment.models.RealmContact.class);
        long tableNativePtr = table.getNativePtr();
        RealmContactColumnInfo columnInfo = (RealmContactColumnInfo) realm.getSchema().getColumnInfo(com.dukeinnovate.android.fragment2fragment.models.RealmContact.class);
        long pkColumnIndex = columnInfo.idIndex;
        com.dukeinnovate.android.fragment2fragment.models.RealmContact object = null;
        while (objects.hasNext()) {
            object = (com.dukeinnovate.android.fragment2fragment.models.RealmContact) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = Table.NO_MATCH;
            Object primaryKeyValue = ((com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface) object).realmGet$id();
            if (primaryKeyValue != null) {
                rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, (String)primaryKeyValue);
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
            } else {
                Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
            }
            cache.put(object, rowIndex);
            String realmGet$name = ((com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface) object).realmGet$name();
            if (realmGet$name != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.nameIndex, rowIndex, realmGet$name, false);
            }
            String realmGet$phoneNumber = ((com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface) object).realmGet$phoneNumber();
            if (realmGet$phoneNumber != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.phoneNumberIndex, rowIndex, realmGet$phoneNumber, false);
            }
            String realmGet$title = ((com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface) object).realmGet$title();
            if (realmGet$title != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.titleIndex, rowIndex, realmGet$title, false);
            }
            String realmGet$email = ((com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface) object).realmGet$email();
            if (realmGet$email != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.emailIndex, rowIndex, realmGet$email, false);
            }
            Long realmGet$integer = ((com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface) object).realmGet$integer().get();
            if (realmGet$integer != null) {
                Table.nativeSetLong(tableNativePtr, columnInfo.integerIndex, rowIndex, realmGet$integer.longValue(), false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, com.dukeinnovate.android.fragment2fragment.models.RealmContact object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex();
        }
        Table table = realm.getTable(com.dukeinnovate.android.fragment2fragment.models.RealmContact.class);
        long tableNativePtr = table.getNativePtr();
        RealmContactColumnInfo columnInfo = (RealmContactColumnInfo) realm.getSchema().getColumnInfo(com.dukeinnovate.android.fragment2fragment.models.RealmContact.class);
        long pkColumnIndex = columnInfo.idIndex;
        long rowIndex = Table.NO_MATCH;
        Object primaryKeyValue = ((com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface) object).realmGet$id();
        if (primaryKeyValue != null) {
            rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, (String)primaryKeyValue);
        }
        if (rowIndex == Table.NO_MATCH) {
            rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
        }
        cache.put(object, rowIndex);
        String realmGet$name = ((com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface) object).realmGet$name();
        if (realmGet$name != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.nameIndex, rowIndex, realmGet$name, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.nameIndex, rowIndex, false);
        }
        String realmGet$phoneNumber = ((com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface) object).realmGet$phoneNumber();
        if (realmGet$phoneNumber != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.phoneNumberIndex, rowIndex, realmGet$phoneNumber, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.phoneNumberIndex, rowIndex, false);
        }
        String realmGet$title = ((com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface) object).realmGet$title();
        if (realmGet$title != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.titleIndex, rowIndex, realmGet$title, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.titleIndex, rowIndex, false);
        }
        String realmGet$email = ((com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface) object).realmGet$email();
        if (realmGet$email != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.emailIndex, rowIndex, realmGet$email, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.emailIndex, rowIndex, false);
        }
        Long realmGet$integer = ((com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface) object).realmGet$integer().get();
        if (realmGet$integer != null) {
            Table.nativeSetLong(tableNativePtr, columnInfo.integerIndex, rowIndex, realmGet$integer.longValue(), false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.integerIndex, rowIndex, false);
        }
        return rowIndex;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.dukeinnovate.android.fragment2fragment.models.RealmContact.class);
        long tableNativePtr = table.getNativePtr();
        RealmContactColumnInfo columnInfo = (RealmContactColumnInfo) realm.getSchema().getColumnInfo(com.dukeinnovate.android.fragment2fragment.models.RealmContact.class);
        long pkColumnIndex = columnInfo.idIndex;
        com.dukeinnovate.android.fragment2fragment.models.RealmContact object = null;
        while (objects.hasNext()) {
            object = (com.dukeinnovate.android.fragment2fragment.models.RealmContact) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getIndex());
                continue;
            }
            long rowIndex = Table.NO_MATCH;
            Object primaryKeyValue = ((com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface) object).realmGet$id();
            if (primaryKeyValue != null) {
                rowIndex = Table.nativeFindFirstString(tableNativePtr, pkColumnIndex, (String)primaryKeyValue);
            }
            if (rowIndex == Table.NO_MATCH) {
                rowIndex = OsObject.createRowWithPrimaryKey(table, pkColumnIndex, primaryKeyValue);
            }
            cache.put(object, rowIndex);
            String realmGet$name = ((com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface) object).realmGet$name();
            if (realmGet$name != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.nameIndex, rowIndex, realmGet$name, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.nameIndex, rowIndex, false);
            }
            String realmGet$phoneNumber = ((com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface) object).realmGet$phoneNumber();
            if (realmGet$phoneNumber != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.phoneNumberIndex, rowIndex, realmGet$phoneNumber, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.phoneNumberIndex, rowIndex, false);
            }
            String realmGet$title = ((com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface) object).realmGet$title();
            if (realmGet$title != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.titleIndex, rowIndex, realmGet$title, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.titleIndex, rowIndex, false);
            }
            String realmGet$email = ((com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface) object).realmGet$email();
            if (realmGet$email != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.emailIndex, rowIndex, realmGet$email, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.emailIndex, rowIndex, false);
            }
            Long realmGet$integer = ((com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface) object).realmGet$integer().get();
            if (realmGet$integer != null) {
                Table.nativeSetLong(tableNativePtr, columnInfo.integerIndex, rowIndex, realmGet$integer.longValue(), false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.integerIndex, rowIndex, false);
            }
        }
    }

    public static com.dukeinnovate.android.fragment2fragment.models.RealmContact createDetachedCopy(com.dukeinnovate.android.fragment2fragment.models.RealmContact realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.dukeinnovate.android.fragment2fragment.models.RealmContact unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new com.dukeinnovate.android.fragment2fragment.models.RealmContact();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.dukeinnovate.android.fragment2fragment.models.RealmContact) cachedObject.object;
            }
            unmanagedObject = (com.dukeinnovate.android.fragment2fragment.models.RealmContact) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface unmanagedCopy = (com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface) unmanagedObject;
        com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface realmSource = (com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$name(realmSource.realmGet$name());
        unmanagedCopy.realmSet$id(realmSource.realmGet$id());
        unmanagedCopy.realmSet$phoneNumber(realmSource.realmGet$phoneNumber());
        unmanagedCopy.realmSet$title(realmSource.realmGet$title());
        unmanagedCopy.realmSet$email(realmSource.realmGet$email());
        unmanagedCopy.realmGet$integer().set(realmSource.realmGet$integer().get());

        return unmanagedObject;
    }

    static com.dukeinnovate.android.fragment2fragment.models.RealmContact update(Realm realm, com.dukeinnovate.android.fragment2fragment.models.RealmContact realmObject, com.dukeinnovate.android.fragment2fragment.models.RealmContact newObject, Map<RealmModel, RealmObjectProxy> cache) {
        com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface realmObjectTarget = (com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface) realmObject;
        com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface realmObjectSource = (com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface) newObject;
        realmObjectTarget.realmSet$name(realmObjectSource.realmGet$name());
        realmObjectTarget.realmSet$phoneNumber(realmObjectSource.realmGet$phoneNumber());
        realmObjectTarget.realmSet$title(realmObjectSource.realmGet$title());
        realmObjectTarget.realmSet$email(realmObjectSource.realmGet$email());
        realmObjectTarget.realmGet$integer().set(realmObjectSource.realmGet$integer().get());
        return realmObject;
    }

    @Override
    @SuppressWarnings("ArrayToString")
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("RealmContact = proxy[");
        stringBuilder.append("{name:");
        stringBuilder.append(realmGet$name());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{id:");
        stringBuilder.append(realmGet$id());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{phoneNumber:");
        stringBuilder.append(realmGet$phoneNumber());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{title:");
        stringBuilder.append(realmGet$title());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{email:");
        stringBuilder.append(realmGet$email());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{integer:");
        stringBuilder.append(realmGet$integer().get());
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long rowIndex = proxyState.getRow$realm().getIndex();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (rowIndex ^ (rowIndex >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxy aRealmContact = (com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxy)o;

        String path = proxyState.getRealm$realm().getPath();
        String otherPath = aRealmContact.proxyState.getRealm$realm().getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aRealmContact.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getIndex() != aRealmContact.proxyState.getRow$realm().getIndex()) return false;

        return true;
    }
}
