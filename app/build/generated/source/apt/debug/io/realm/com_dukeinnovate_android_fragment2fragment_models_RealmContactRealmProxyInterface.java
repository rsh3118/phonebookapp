package io.realm;


public interface com_dukeinnovate_android_fragment2fragment_models_RealmContactRealmProxyInterface {
    public String realmGet$name();
    public void realmSet$name(String value);
    public String realmGet$id();
    public void realmSet$id(String value);
    public String realmGet$phoneNumber();
    public void realmSet$phoneNumber(String value);
    public String realmGet$title();
    public void realmSet$title(String value);
    public String realmGet$email();
    public void realmSet$email(String value);
    public MutableRealmInteger realmGet$integer();
}
